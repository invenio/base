# Base Docker images for Invenio

This repository contains the main base Docker image for Invenio, which has as base image `CentOS 7`. It also installs `npm`, a few needed packages and set the correct `locale`.
The base images is pushed to the GitLab registry `invenio/base` and tagged with the name `centos7`.

## Available images

There are 3 base images available:
* `invenio/base:python2`: install `python 2` and `pip`
* `invenio/base:python3`: install `python 3.5` and `pip`
* `invenio/base:python2-xrootd`: install `python 2`, `pip` and [XRootD](http://xrootd.org/)

The `python2-xrootd` images expect an argument `XROOTD_VERSION`, which corresponds to the XRootD version to install. By default, it installs version `4.7.1`, which is known to be working.
Please notice that `python-xrootd` does not support Python 3 at the moment (see [ official documentation](https://github.com/xrootd/xrootd-python)).

## Builds

Images are re-built every night, to ensure that latest patches from CentOS are fetched.

## Contribute

New images pushed to the GitLab registry are used by **several** Invenio installations! Be sure that images are correctly working when you modify them.
